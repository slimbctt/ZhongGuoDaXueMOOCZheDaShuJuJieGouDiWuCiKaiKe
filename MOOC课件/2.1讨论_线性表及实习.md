# 第2讲讨论：链式存储中FindKth函数的另一种实现？

如果将链式存储中FindKth的函数实现（如下）做个修改：把函数最后的if语句判断条件改为判断p是否为NULL，即：
```c
      if (p==NULL) return NULL;
      else return p;
```
或者说直接简化为：`return p;`

对于这样的修改，程序还正确吗？为什么？
```c
List *FindKth( int K, List *PtrL )
{     List  *p = PtrL;
       int  i = 1;
       while (p !=NULL && i < K ){
              p = p->Next;
              i++;
       }
       if(i == K) return p;    /*找到第K个，返回指针*/
       else  return NULL;    /* 否则返回空 */
}
```

##  何钦铭老师解析

我们要比较的两种方法称为方法1(即视频中介绍的方法)和方法2(讨论的方法，直接return p).

从while循环和return的条件看，我们要考虑的情况主要有：p是否等于NULL，以及i与k的大小关系。所以，可以分以下6种情况，每种情况我们都很容易看到方法1和方法2的返回结果。

这样一比较，我们就可以发现：当i>k且p!=NULL时，这两种方法的返回结果是不一样的。

![真值表](http://img1.ph.126.net/hJEUIn-NkTRd-W-oG9Bgmw==/2395352051825511379.jpg)
