#include <cstdio>
const int MAXN=100010; 

int Max3( int A, int B, int C )
{ /* 返回3个整数中的最大值 */
    return A > B ? A > C ? A : C : B > C ? B : C;
}

int DivideAndConquer(int List[], int left, int right)
{
	if(left==right)
	{
		if(List[left]>0) return List[left];
		return 0;
	}
	
	int center = (left+right)/2,
	MaxLeftSum = DivideAndConquer(List,left,center),
	MaxRightrSum = DivideAndConquer(List,center+1,right);
	
	int tmp;
	int MaxLeftBorderSum=tmp=0;
	for(int i=center+1;i<=right;i++)
	{
		tmp += List[i];
		if(tmp > MaxLeftBorderSum) MaxLeftBorderSum=tmp;
	}
	
	int MaxRightBorderSum=tmp=0;
	for(int i=center;i>=left;i--)
	{
		tmp += List[i];
		if(tmp > MaxRightBorderSum) MaxRightBorderSum=tmp;
	}
	
	return Max3(MaxLeftSum, MaxRightrSum, MaxLeftBorderSum+MaxRightBorderSum);
}

int MaxSubseqSum3(int List[], int N)
{
	return DivideAndConquer(List, 0, N-1);
}

int MaxSubseqSum4(int List[], int N)
{
	int thisSum, maxSum;
	thisSum = maxSum = 0;
	for(int i=0;i<N;i++)
	{
		thisSum += List[i];
		if(thisSum > maxSum) maxSum = thisSum;
		else if(thisSum < 0)
		    thisSum = 0;
	}
	return maxSum;
}

int main(void)
{
	freopen("D:/jahentao/Documents/Courses/考研/浙大878复习资料/中国大学MOOC浙大《数据结构》第五次开课/code/PTA/01.txt","r",stdin);
	
    int N, List[MAXN];
	scanf("%d",&N);
	for(int i=0;i<N;i++)
		scanf("%d",&List[i]);
	printf("%d\n",MaxSubseqSum4(List, N));
}
